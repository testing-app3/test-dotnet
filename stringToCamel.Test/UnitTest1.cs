namespace stringToCamel.Test;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        Assert.That(stringToCamel.App.Program.ToCamelCase("this is a string"), Is.EqualTo("thisIsAString"));
    }

    [Test]
    public void Test2()
    {
        Assert.That(stringToCamel.App.Program.ToCamelCase("this_is_a_string"), Is.EqualTo("thisIsAString"));
    }
}