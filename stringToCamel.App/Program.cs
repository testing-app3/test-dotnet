﻿namespace stringToCamel.App{
public class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Enter a string to convert");

        string? userInput = Console.ReadLine();

        if (userInput == null)
        {
            return;
        }

        Console.WriteLine(ToCamelCase(userInput));
    }

    public static string ToCamelCase(string str){
        string tempOut = string.Empty;

        var tempstring = str.Split(" ");

        for (int i = 0; i < tempstring.Length; i++)
            {
                if (i == 0)
                {
                    tempOut += char.ToLower(tempstring[i][0]) + tempstring[i].Substring(1);
                }
                else
                {
                    tempOut += char.ToUpper(tempstring[i][0]) + tempstring[i].Substring(1);
                }

            }
        return tempOut;
    }
}
}
